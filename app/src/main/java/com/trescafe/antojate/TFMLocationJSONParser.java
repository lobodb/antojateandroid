package com.trescafe.antojate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lobo_d_b on 11/14/14.
 */
public class TFMLocationJSONParser {
    public static List<TFMLocation> parseFeed(String content){
        try{
            JSONArray ar = new JSONArray(content);
            List<TFMLocation> tfmLocationLIST = new ArrayList<TFMLocation>();

            for(int i=0; i < ar.length(); i++){
                JSONObject obj = ar.getJSONObject(i);
                TFMLocation tfmLocation = new TFMLocation();
                JSONObject objid = obj.getJSONObject("id");
                tfmLocation.setId(objid.getString("$oid"));
                tfmLocation.setName(obj.getString("name"));
                tfmLocationLIST.add(tfmLocation);
            }
            return tfmLocationLIST;
        }catch (JSONException e){
            e.printStackTrace();
            return null;

        }

    }
}
