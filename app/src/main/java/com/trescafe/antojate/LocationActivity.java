package com.trescafe.antojate;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class LocationActivity extends ActionBarActivity {

    private Context locationContext;


    List<TFMLocation> TFMLocations = new ArrayList<TFMLocation>();
    ListView tfmLocationsListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        android.support.v7.app.ActionBar ab = getSupportActionBar();
        ab.setTitle("Selecciona tu barrio");
        ab.setDisplayShowHomeEnabled(true);
        ab.setLogo(R.drawable.ic_launcher);
        ab.setDisplayUseLogoEnabled(true);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        String clickedCity = getIntent().getStringExtra("<clickedCity>");

        if(isOnline()){
            String external_url = ((Antojate) this.getApplication() ).getExternal_url();
            String content = HttpManager.getData(external_url+"/locations_mobile/"+ clickedCity +".json");

            try{
                TFMLocations = TFMLocationJSONParser.parseFeed(content);
            }catch (Exception e){
                TFMLocation location = new TFMLocation("Hubo un error en el sistema, estamos trabajando para solucionarlo");
                TFMLocations.add(location);
            }

        }else{
            TFMLocation location = new TFMLocation("Verifique su conexión a internet");
            TFMLocations.add(location);
        }


        locationContext = this;





        tfmLocationsListView = (ListView) findViewById(R.id.TFMLocationsLV);
        ArrayAdapter<TFMLocation> adapter = new TFMLocationListAdapter();
        tfmLocationsListView.setAdapter(adapter);

        tfmLocationsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TFMLocation item = (TFMLocation) tfmLocationsListView.getItemAtPosition(position);
                try{
                    Intent location = new Intent(locationContext, SubsidiaryActivity.class).putExtra("<clickedLocation>", item.getId().toString());
                    startActivity(location);
                }catch(Exception e){
                    System.out.println("no hay internet");
                }

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_location, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected boolean isOnline(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if(netInfo != null && netInfo.isConnectedOrConnecting()){
            return true;
        }else{
            return false;
        }
    }

    private class TFMLocationListAdapter extends ArrayAdapter<TFMLocation>{
        public TFMLocationListAdapter(){
            super (LocationActivity.this, R.layout.tfmlocationslistview_item, TFMLocations);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent){
            view = getLayoutInflater().inflate(R.layout.tfmlocationslistview_item, parent, false);
            TFMLocation currentLocation = TFMLocations.get(position);

            TextView name = (TextView) view.findViewById(R.id.textviewlocationname);
            name.setText(currentLocation.getName());

            return view;
        }
    }
}
