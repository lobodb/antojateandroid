package com.trescafe.antojate;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class ProductActivity extends ActionBarActivity {

    List<TFMProduct> TFMProducts = new ArrayList<TFMProduct>();
    ListView tfmProductsListView;


    private Context productContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        android.support.v7.app.ActionBar ab = getSupportActionBar();
        ab.setTitle("Selecciona tus productos");
        ab.setDisplayShowHomeEnabled(true);
        ab.setLogo(R.drawable.ic_launcher);
        ab.setDisplayUseLogoEnabled(true);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        String clickedSubsidiary = getIntent().getStringExtra("<clickedSubsidiary>");

        if(isOnline()){
            String external_url = ((Antojate) this.getApplication() ).getExternal_url();
            String content = HttpManager.getData(external_url+"/admin/products_mobile/"+ clickedSubsidiary +".json");

            try{
            TFMProducts = TFMProductJSONParser.parseFeed(content);
            }catch (Exception e){
                TFMProduct product = new TFMProduct("Hubo un error en el sistema, estamos trabajando para solucionarlo");
                TFMProducts.add(product);
            }

        }else{
            TFMProduct product = new TFMProduct("Verifique su conexión a internet");
            TFMProducts.add(product);
        }

        productContext = this;

        tfmProductsListView = (ListView) findViewById(R.id.TFMProductsLV);
        ArrayAdapter<TFMProduct> adapter = new TFMProductListAdapter();
        tfmProductsListView.setAdapter(adapter);

        tfmProductsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TFMProduct item = (TFMProduct) tfmProductsListView.getItemAtPosition(position);
                System.out.println(item.getName());
                try{
                Intent size = new Intent(productContext, SizeActivity.class).putExtra("<clickedProduct>", item.getId().toString());
                startActivity(size);
                }catch(Exception e){
                    System.out.println("no hay internet");
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_product, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected boolean isOnline(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if(netInfo != null && netInfo.isConnectedOrConnecting()){
            return true;
        }else{
            return false;
        }
    }

    private class TFMProductListAdapter extends ArrayAdapter<TFMProduct>{
        public TFMProductListAdapter(){
            super (ProductActivity.this, R.layout.tfmproductslistview_item, TFMProducts);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent){
            view = getLayoutInflater().inflate(R.layout.tfmproductslistview_item, parent, false);
            TFMProduct currentProduct = TFMProducts.get(position);

            String external_url = ((Antojate) ProductActivity.this.getApplication() ).getExternal_url();
            ImageView image = (ImageView) view.findViewById(R.id.imageViewproductimage);
            Picasso.with(productContext).load(external_url + currentProduct.getImageurl()).into(image);

            TextView name = (TextView) view.findViewById(R.id.textviewproductname);
            name.setText(currentProduct.getName());


            return view;
        }
    }
}
