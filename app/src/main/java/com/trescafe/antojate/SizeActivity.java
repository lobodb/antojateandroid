package com.trescafe.antojate;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class SizeActivity extends ActionBarActivity {

    List<TFMSize> TFMSizes = new ArrayList<TFMSize>();
    ListView tfmSizesListView;
    TextView textviewproductsizename;


    private Context sizeContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_size);

        android.support.v7.app.ActionBar ab = getSupportActionBar();
        ab.setTitle("Selecciona algún tamaño");
        ab.setDisplayShowHomeEnabled(true);
        ab.setLogo(R.drawable.ic_launcher);
        ab.setDisplayUseLogoEnabled(true);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        String clickedProduct = getIntent().getStringExtra("<clickedProduct>");

        if(isOnline()){
            String external_url = ((Antojate) this.getApplication() ).getExternal_url();
            String content = HttpManager.getData(external_url+"/admin/sizes_mobile/"+ clickedProduct +".json");

            try{
                TFMSizes = TFMSizeJSONParser.parseFeed(content);
            }catch (Exception e){
                TFMSize size = new TFMSize("Hubo un error en el sistema, estamos trabajando para solucionarlo");
                TFMSizes.add(size);
            }

        }else{
            TFMSize size = new TFMSize("Verifique su conexión a internet");
            TFMSizes.add(size);
        }

        sizeContext = this;

        textviewproductsizename = (TextView) findViewById(R.id.textviewproductsizename);
        textviewproductsizename.setText(TFMSizes.get(0).getProduct());

        tfmSizesListView = (ListView) findViewById(R.id.TFMSizesLV);
        ArrayAdapter<TFMSize> adapter = new TFMSizeListAdapter();
        tfmSizesListView.setAdapter(adapter);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_size, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void reviewOrder(View view) {
        Intent revieworder = new Intent(sizeContext, ReviewOrderActivity.class);
        startActivity(revieworder);
    }

    protected boolean isOnline(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if(netInfo != null && netInfo.isConnectedOrConnecting()){
            return true;
        }else{
            return false;
        }
    }

    private class TFMSizeListAdapter extends ArrayAdapter<TFMSize>{
        public TFMSizeListAdapter(){
            super (SizeActivity.this, R.layout.tfmsizeslistview_item, TFMSizes);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent){
            view = getLayoutInflater().inflate(R.layout.tfmsizeslistview_item, parent, false);
            final TFMSize currentSize = TFMSizes.get(position);


            String external_url = ((Antojate) SizeActivity.this.getApplication() ).getExternal_url();

            final CheckBox checkbox = (CheckBox) view.findViewById(R.id.checkboxsize);


            TextView sizenameprice = (TextView) view.findViewById(R.id.textviewsizenameprice);
            sizenameprice.setText(currentSize.getName()+"\n$"+currentSize.getPrice());

            final NumberPicker numberpicker = (NumberPicker) view.findViewById(R.id.numberpickersize);

            String[] nums = new String[5];
            for(int i=0; i<nums.length; i++)
                nums[i] = Integer.toString(i+1);

            numberpicker.setMinValue(1);
            numberpicker.setMaxValue(5);
            numberpicker.setVerticalScrollBarEnabled(true);
            numberpicker.setDisplayedValues(nums);
            numberpicker.setValue(1);

            if( ((Antojate) SizeActivity.this.getApplication() ).existance(currentSize.getId()) ){
                checkbox.setChecked(true);

                numberpicker.setValue( ((Antojate) SizeActivity.this.getApplication() ).getCantidad(currentSize.getId()) );
            }

            checkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if( checkbox.isChecked() ){
                        ((Antojate) SizeActivity.this.getApplication() ).addToOrder(currentSize.getId(), numberpicker.getValue(), currentSize.getName(), currentSize.getProduct(), currentSize.getPrice());

                    }else{
                        ((Antojate) SizeActivity.this.getApplication() ).removeFromOrder(currentSize.getId());
                    }

                    System.out.println("///");
                    ((Antojate) SizeActivity.this.getApplication() ).imprimirOrden();
                    System.out.println("///");

                }
            });


            return view;
        }
    }
}
