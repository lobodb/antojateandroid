package com.trescafe.antojate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lobo_d_b on 11/14/14.
 */
public class TFMSizeJSONParser {
    public static List<TFMSize> parseFeed(String content){
        try{
            JSONArray ar = new JSONArray(content);
            List<TFMSize> tfmSizeLIST = new ArrayList<TFMSize>();

            for(int i=0; i < ar.length(); i++){
                JSONObject obj = ar.getJSONObject(i);
                TFMSize tfmSize = new TFMSize();
                JSONObject objid = obj.getJSONObject("id");
                tfmSize.setId(objid.getString("$oid"));
                tfmSize.setName(obj.getString("name"));
                tfmSize.setPrice(obj.getString("price"));
                tfmSize.setProduct(obj.getString("product"));
                tfmSizeLIST.add(tfmSize);
            }
            return tfmSizeLIST;
        }catch (JSONException e){
            e.printStackTrace();
            return null;

        }
    }
}
