package com.trescafe.antojate;

import android.content.Context;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class ReviewOrderActivity extends ActionBarActivity {

    List<TFMSize> TFMSizes = new ArrayList<TFMSize>();
    ListView tfmReviewOrderListView;

    private Context revieworderContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_order);

        android.support.v7.app.ActionBar ab = getSupportActionBar();
        ab.setTitle("Revisa tu pedido");
        ab.setDisplayShowHomeEnabled(true);
        ab.setLogo(R.drawable.ic_launcher);
        ab.setDisplayUseLogoEnabled(true);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        TFMSizes = ((Antojate) ReviewOrderActivity.this.getApplication() ).getSizes();

        revieworderContext = this;
        tfmReviewOrderListView = (ListView) findViewById(R.id.TFMReviewOrderLV);
        ArrayAdapter<TFMSize> adapter = new TFMReviewOrderListAdapter();
        tfmReviewOrderListView.setAdapter(adapter);

        TextView total = (TextView) findViewById(R.id.textviewtotal);
        total.setText("Total $" + ((Antojate) ReviewOrderActivity.this.getApplication()).getTotal() );


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_review_order, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void doOrder(View view) {
        Intent order = new Intent(revieworderContext, OrderActivity.class);
        startActivity(order);
    }


    private class TFMReviewOrderListAdapter extends ArrayAdapter<TFMSize>{
        public TFMReviewOrderListAdapter(){
            super (ReviewOrderActivity.this, R.layout.tfmrevieworderlistview_item, TFMSizes);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent){
            view = getLayoutInflater().inflate(R.layout.tfmrevieworderlistview_item, parent, false);
            final TFMSize currentSize = TFMSizes.get(position);


            String external_url = ((Antojate) ReviewOrderActivity.this.getApplication() ).getExternal_url();

            TextView name = (TextView) view.findViewById(R.id.textvieworderproduct);
            name.setText( currentSize.getProduct() + "\n" +
                    currentSize.getCantidad() + " " + currentSize.getName() + "\n" +
                    "$" + ( Float.valueOf( currentSize.getPrice() ) * currentSize.getCantidad() ) );




            return view;
        }
    }
}
