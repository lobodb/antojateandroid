package com.trescafe.antojate;

import android.app.Application;

import java.util.ArrayList;

/**
 * Created by lobo_d_b on 3/2/15.
 */
public class Antojate extends Application {
    private String external_url;
    private ArrayList<TFMSize> sizes = new ArrayList<TFMSize>();
    private String gcmRegId;


    //url del servidor
    public Antojate(){
        this.external_url = "http://192.168.0.142:3000";
    }

    public String getExternal_url() {
        return external_url;
    }

    public void setExternal_url(String external_url) {
        this.external_url = external_url;
    }

    public void addToOrder(String id, int cantidad, String nombre, String product, String price){
        TFMSize size = new TFMSize();
        size.setId(id);
        size.setCantidad(cantidad);
        size.setName(nombre);
        size.setProduct(product);
        size.setPrice(price);
        this.getSizes().add(size);
    }

    public void removeFromOrder(String id){
        for (int i = 0; i < this.getSizes().size(); i++) {
            TFMSize size = new TFMSize();

            size = this.getSizes().get(i);
            if(size.getId().equals(id))
                this.getSizes().remove(i);
        }
    }

    public void imprimirOrden(){
        for(TFMSize size : this.getSizes()) {
            System.out.println(size.getId() + " " + size.getCantidad() + " " + size.getName());
            System.out.println( " " + getSizes().size() );
        }

    }

    public int getCantidad(String id){
        int cantidad = 1;
        for (int i = 0; i < this.getSizes().size(); i++) {
            TFMSize size = new TFMSize();

            size = this.getSizes().get(i);
            if(size.getId().equals(id))
                cantidad = size.getCantidad();

        }
        return cantidad;


    }

    public boolean existance(String id){

        boolean existe = false;

        for (int i = 0; i < this.getSizes().size(); i++) {
            TFMSize size;

            size = this.getSizes().get(i);


            if( size.getId().equals(id) ) {
                existe = true;
            }
        }
        return existe;

    }

    public ArrayList<TFMSize> getSizes() {
        return sizes;
    }

    public String getTotal(){
        float total = 0;
        for(TFMSize size : this.getSizes()) {
            total += size.getCantidad() * Float.valueOf(size.getPrice());
        }
        return total+"";
    }

    public String getGcmRegId() {
        return gcmRegId;
    }

    public void setGcmRegId(String gcmRegId) {
        this.gcmRegId = gcmRegId;
    }


}
