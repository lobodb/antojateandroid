package com.trescafe.antojate;


import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lobo_d_b on 11/14/14.
 */
public class HttpManager {
    public static String getData(String uri){
        System.out.println("get data uri");
        System.out.println(uri);
        BufferedReader reader = null;

        try{
            URL url = new URL(uri);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            StringBuilder sb = new StringBuilder();


            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));

            String line;
            while ((line = reader.readLine()) != null ){
                sb.append(line+"\n");
            }

            return sb.toString();

        }catch (Exception e){

            e.printStackTrace();
            return  null;
        }finally {
            if(reader != null){
                try{
                    reader.close();
                }catch (IOException e){
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

    public static boolean postData(String uri, String phone, String address, String comments, ArrayList<TFMSize> order, String gcmRegId){
        boolean envio = false;
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(uri);
        try {
            // Add your data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("phone", phone));
            nameValuePairs.add(new BasicNameValuePair("address", address));
            nameValuePairs.add(new BasicNameValuePair("comments", comments));
            nameValuePairs.add(new BasicNameValuePair("gcmRegId", gcmRegId));

            int i = 0;
            for(TFMSize size : order) {

                nameValuePairs.add(new BasicNameValuePair("id"+i, size.getId()));
                nameValuePairs.add(new BasicNameValuePair("quantity"+i, String.valueOf(size.getCantidad())));
                i++;
            }

            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);
            if ( response.getStatusLine().getStatusCode() == 200 ){
                return true;
            }
            else{
                return false;
            }


        } catch (ClientProtocolException e) {
            return false;
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
            return false;
        }


    }
}

