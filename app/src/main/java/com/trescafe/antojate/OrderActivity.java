package com.trescafe.antojate;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;


public class OrderActivity extends ActionBarActivity {

    private Context orderContext;
    EditText phoneet;
    EditText direccionet;
    EditText observacioneset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        android.support.v7.app.ActionBar ab = getSupportActionBar();
        ab.setTitle("¿Cuáles son tus datos?");
        ab.setDisplayShowHomeEnabled(true);
        ab.setLogo(R.drawable.ic_launcher);
        ab.setDisplayUseLogoEnabled(true);

        orderContext = this;

        phoneet = (EditText) findViewById(R.id.edittextorderphone);
        direccionet = (EditText) findViewById(R.id.edittextorderdireccion);
        observacioneset = (EditText) findViewById(R.id.edittextorderobservaciones);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_order, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void sendOrder(View view) {

        if(isOnline()){

            String external_url = ((Antojate) this.getApplication() ).getExternal_url();

            String phone;
            String direccion;
            String observaciones;
            String gcmRegId;

            phone = phoneet.getText().toString();
            direccion = direccionet.getText().toString();
            observaciones = observacioneset.getText().toString();

            ArrayList<TFMSize> order = ((Antojate) this.getApplication() ).getSizes();
            gcmRegId = ((Antojate) this.getApplication()).getGcmRegId();

            HttpManager httpManager = new HttpManager();

            if ( httpManager.postData(external_url+"/post_order", phone, direccion, observaciones, order, gcmRegId) ){
                Intent waitconfirmation = new Intent(orderContext, WaitConfirmationActivity.class).putExtra("<message>", "Espera una notificación con la confirmación de tu pedido");
                startActivity(waitconfirmation);
            }else{
                Intent waitconfirmation = new Intent(orderContext, WaitConfirmationActivity.class).putExtra("<message>", "Hubo un problema al realizar la orden, por favor inténtalo de nuevo");
                startActivity(waitconfirmation);
            }

            System.out.println("phone "+ phone + " direccion " + direccion + order + "  observaciones" + observaciones + "gcmRegId" + gcmRegId);

        }else{

        }
    }

    protected boolean isOnline(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if(netInfo != null && netInfo.isConnectedOrConnecting()){
            return true;
        }else{
            return false;
        }
    }
}
