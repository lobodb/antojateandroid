package com.trescafe.antojate;

import java.util.LinkedList;

/**
 * Created by lobo_d_b on 2/27/15.
 */
public class TFMProduct {

    private String id;
    private String name;
    private String imageurl;

    public TFMProduct() { }

    public TFMProduct(String name) { this.setName(name); }

    public String getId() { return id; }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }
}
