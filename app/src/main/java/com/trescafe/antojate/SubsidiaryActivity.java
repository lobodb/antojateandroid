package com.trescafe.antojate;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class SubsidiaryActivity extends ActionBarActivity {

    private Context subsidiaryContext;


    List<TFMSubsidiary> TFMSubsidiaries = new ArrayList<TFMSubsidiary>();
    ListView tfmSubsidiariesListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subsidiary);

        android.support.v7.app.ActionBar ab = getSupportActionBar();
        ab.setTitle("Escoge restaurante");
        ab.setDisplayShowHomeEnabled(true);
        ab.setLogo(R.drawable.ic_launcher);
        ab.setDisplayUseLogoEnabled(true);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        String clickedLocation = getIntent().getStringExtra("<clickedLocation>");

        if(isOnline()){
            String external_url = ((Antojate) this.getApplication() ).getExternal_url();
            String content = HttpManager.getData(external_url+"/admin/subsidiaries_mobile/"+ clickedLocation +".json");


            TFMSubsidiaries = TFMSubsidiaryJSONParser.parseFeed(content);
        }else{
            TFMSubsidiary subsidiary = new TFMSubsidiary(":'(");
            TFMSubsidiaries.add(subsidiary);
        }

        subsidiaryContext = this;

        tfmSubsidiariesListView = (ListView) findViewById(R.id.TFMSubsidiariesLV);
        ArrayAdapter<TFMSubsidiary> adapter = new TFMSubsidiaryListAdapter();
        tfmSubsidiariesListView.setAdapter(adapter);

        tfmSubsidiariesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TFMSubsidiary item = (TFMSubsidiary) tfmSubsidiariesListView.getItemAtPosition(position);
                System.out.println(item.getName());
                try{
                Intent product = new Intent(subsidiaryContext, ProductActivity.class).putExtra("<clickedSubsidiary>", item.getId().toString());
                startActivity(product);
                }catch(Exception e){
                    System.out.println("no hay internet");
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_subsidiary, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected boolean isOnline(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if(netInfo != null && netInfo.isConnectedOrConnecting()){
            return true;
        }else{
            return false;
        }
    }

    private class TFMSubsidiaryListAdapter extends ArrayAdapter<TFMSubsidiary>{
        public TFMSubsidiaryListAdapter(){
            super (SubsidiaryActivity.this, R.layout.tfmsubsidiarieslistview_item, TFMSubsidiaries);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent){
            view = getLayoutInflater().inflate(R.layout.tfmsubsidiarieslistview_item, parent, false);
            TFMSubsidiary currentSubsidiary = TFMSubsidiaries.get(position);

            TextView name = (TextView) view.findViewById(R.id.textviewsubsidiaryname);
            name.setText(currentSubsidiary.getName());

            TextView attention = (TextView) view.findViewById(R.id.textviewsubsidiaryattention);
            attention.setText("Despachos: " + currentSubsidiary.getOpenTime() + " - " + currentSubsidiary.getCloseTime());

            String external_url = ((Antojate) SubsidiaryActivity.this.getApplication() ).getExternal_url();
            ImageView image = (ImageView) view.findViewById(R.id.imageviewsubsidiaryimage);

            Picasso.with(subsidiaryContext).load(external_url + currentSubsidiary.getImageurl()).into(image);

            return view;
        }
    }
}
