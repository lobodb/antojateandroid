package com.trescafe.antojate;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class CityActivity extends ActionBarActivity {

    List<TFMCity> TFMCities = new ArrayList<TFMCity>();
    ListView tfmCitiesListView;

    GoogleCloudMessaging gcm;
    String regid;
    String PROJECT_NUMBER = "299983549389";


    private Context cityContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        android.support.v7.app.ActionBar ab = getSupportActionBar();
        //Typeface font = Typeface.createFromAsset(getAssets(), "coquette-regular.ttf");
        ab.setTitle("¿En qué ciudad vives?");
        ab.setLogo(R.drawable.ic_launcher);

        ab.setDisplayShowHomeEnabled(true);
        ab.setDisplayUseLogoEnabled(true);


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        if(isOnline()){
            String external_url = ((Antojate) this.getApplication() ).getExternal_url();
            String content = HttpManager.getData(external_url + "/cities.json");
        try{

            getRegId();

            TFMCities = TFMCityJSONParser.parseFeed(content);
        }catch (Exception e){
            TFMCity city = new TFMCity("Hubo un error en el sistema, estamos trabajando para solucionarlo");
            TFMCities.add(city);
        }



        }else{
            TFMCity city = new TFMCity("Verifique su conexión a internet");
            TFMCities.add(city);
        }

        cityContext = this;


        tfmCitiesListView = (ListView) findViewById(R.id.TFMCitiesLV);
        ArrayAdapter<TFMCity> adapter = new TFMCityListAdapter();
        tfmCitiesListView.setAdapter(adapter);


        tfmCitiesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TFMCity item = (TFMCity) tfmCitiesListView.getItemAtPosition(position);
                try{
                    Intent city = new Intent(cityContext, LocationActivity.class).putExtra("<clickedCity>", item.getId().toString());
                    startActivity(city);
                }catch(Exception e){
                    System.out.println("no hay internet");
                }
            }
        });

    }

    public void getRegId(){

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    }
                    regid = gcm.register(PROJECT_NUMBER);
                    msg = "Device registered, registration ID=" + regid;

                    ((Antojate) getApplication()).setGcmRegId(regid);

                    Log.i("GCM", msg);

                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();

                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                System.out.println("emm si, hola"+msg);
            }
        }.execute(null, null, null);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_city, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class TFMCityListAdapter extends ArrayAdapter<TFMCity>{
        public TFMCityListAdapter(){
            super(CityActivity.this, R.layout.tfmcitieslistview_item, TFMCities);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent){
            view = getLayoutInflater().inflate(R.layout.tfmcitieslistview_item, parent, false);
            TFMCity currentCity = TFMCities.get(position);

            TextView name = (TextView) view.findViewById(R.id.textviewcityname);
            name.setText(currentCity.getName());

            return view;
        }
    }

    protected boolean isOnline(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if(netInfo != null && netInfo.isConnectedOrConnecting()){
            return true;
        }else{
            return false;
        }
    }


}
