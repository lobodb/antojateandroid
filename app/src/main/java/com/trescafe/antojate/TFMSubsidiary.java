package com.trescafe.antojate;

/**
 * Created by lobo_d_b on 2/26/15.
 */
public class TFMSubsidiary {
    private String id;
    private String name;
    private String openTime;
    private String closeTime;
    private String imageurl;

    public TFMSubsidiary() { }

    public TFMSubsidiary(String name) { this.setName(name); }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }


    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }
}
