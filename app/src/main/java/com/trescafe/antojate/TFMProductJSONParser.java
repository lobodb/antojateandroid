package com.trescafe.antojate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lobo_d_b on 11/14/14.
 */
public class TFMProductJSONParser {
    public static List<TFMProduct> parseFeed(String content){
        try{
            JSONArray ar = new JSONArray(content);
            List<TFMProduct> tfmProductLIST = new ArrayList<TFMProduct>();

            for(int i=0; i < ar.length(); i++){
                TFMProduct tfmProduct = new TFMProduct();
                JSONObject obj = ar.getJSONObject(i);
                JSONObject objid = obj.getJSONObject("id");
                tfmProduct.setId(objid.getString("$oid"));
                tfmProduct.setName(obj.getString("name"));
                tfmProduct.setImageurl(obj.getString("image_url"));
                tfmProductLIST.add(tfmProduct);
            }
            return tfmProductLIST;
        }catch (JSONException e){
            e.printStackTrace();
            return null;

        }
    }
}
