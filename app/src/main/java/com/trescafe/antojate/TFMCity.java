package com.trescafe.antojate;

/**
 * Created by lobo_d_b on 11/14/14.
 */
public class TFMCity {
    private String id;
    private String name;


    public TFMCity(){ }

    public TFMCity(String name){
        this.setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
