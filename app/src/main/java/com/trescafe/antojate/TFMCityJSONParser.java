package com.trescafe.antojate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lobo_d_b on 11/14/14.
 */
public class TFMCityJSONParser {
    public static List<TFMCity> parseFeed(String content){
        try{
            JSONArray ar = new JSONArray(content);
            List<TFMCity> tfmCityLIST = new ArrayList<TFMCity>();

            for(int i=0; i < ar.length(); i++){
                JSONObject obj = ar.getJSONObject(i);
                TFMCity tfmCity = new TFMCity();
                JSONObject objid = obj.getJSONObject("id");
                tfmCity.setId(objid.getString("$oid"));
                tfmCity.setName(obj.getString("name"));
                tfmCityLIST.add(tfmCity);
            }
            return tfmCityLIST;
        }catch (JSONException e){
            e.printStackTrace();
            return null;

        }
    }
}
