package com.trescafe.antojate;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by lobo_d_b on 6/24/15.
 */
public class GcmMessageHandler extends IntentService {

    String mes;
    private Handler handler;
    public GcmMessageHandler() {
        super("GcmMessageHandler");
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        handler = new Handler();
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();

        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        mes = extras.getString("message");

        Log.i("GCM", "Received : (" +messageType+")  "+ " " + extras.getString("message"));

        PendingIntent pIntent = PendingIntent.getActivity(this, 0 , intent, 0);

        Notification builder = new Notification.Builder(this)
                .setContentTitle("Antójate")
                .setContentText("tu pedido llegará +o- en 45 minutos")
                .setSmallIcon(R.drawable.ic_launcher)
                .setAutoCancel(true)
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        notificationManager.notify(0,builder);


        GcmBroadcastReceiver.completeWakefulIntent(intent);

    }
}
