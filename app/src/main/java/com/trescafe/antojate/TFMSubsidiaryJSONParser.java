package com.trescafe.antojate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lobo_d_b on 11/14/14.
 */
public class TFMSubsidiaryJSONParser {
    public static List<TFMSubsidiary> parseFeed(String content){
        try{
            JSONArray ar = new JSONArray(content);
            List<TFMSubsidiary> tfmSubsidiaryList = new ArrayList<TFMSubsidiary>();

            for(int i=0; i < ar.length(); i++){
                JSONObject obj = ar.getJSONObject(i);
                TFMSubsidiary tfmSubsidiary = new TFMSubsidiary();
                JSONObject objid = obj.getJSONObject("id");
                tfmSubsidiary.setId(objid.getString("$oid"));
                tfmSubsidiary.setName(obj.getString("name"));
                tfmSubsidiary.setImageurl(obj.getString("image_url"));
                tfmSubsidiary.setOpenTime(obj.getString("open"));
                tfmSubsidiary.setCloseTime(obj.getString("close"));
                tfmSubsidiaryList.add(tfmSubsidiary);
            }
            return tfmSubsidiaryList;
        }catch (JSONException e){
            e.printStackTrace();
            return null;

        }

    }
}
